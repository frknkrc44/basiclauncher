package org.frknkrc44.blauncher;

import android.app.*;
import android.content.*;
import android.content.pm.*;
import android.net.*;
import android.os.*;
import android.provider.*;
import android.text.*;
import android.util.*;
import android.view.*;
import android.view.inputmethod.*;
import android.widget.*;

public class MainActivity extends Activity implements ListView.OnItemClickListener, ListView.OnItemLongClickListener, TextWatcher {

	private LauncherApps mLauncherApps;
	private AlertDialog.Builder mAppOptionsDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onRestart();
        setContentView(R.layout.main);
		mLauncherApps = (LauncherApps) getSystemService(LAUNCHER_APPS_SERVICE);
		mLauncherApps.registerCallback(new LauncherAppsCallback());
		final ListView appList = findViewById(android.R.id.list);
		appList.setOnItemClickListener(this);
		appList.setOnItemLongClickListener(this);
		appList.setAdapter(new AppListAdapter(this));
		appList.setFastScrollEnabled(true);
		appListHandler.sendEmptyMessageDelayed(LauncherAppsCallback.UNAVAILABLE,250);
		final EditText et = findViewById(android.R.id.edit);
		et.addTextChangedListener(this);
		et.setOnTouchListener(new View.OnTouchListener(){

				@Override
				public boolean onTouch(View p1, MotionEvent p2) {
					if(p2.getAction() == MotionEvent.ACTION_DOWN){
						appList.smoothScrollToPositionFromTop(0,0,100);
					}
					return false;
				}

		});
		et.setOnFocusChangeListener(new View.OnFocusChangeListener(){

				@Override
				public void onFocusChange(View p1, boolean p2) {
					if(!p2){
						InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE); 
						imm.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
					}
				}

		});
		findViewById(android.R.id.content).setOnTouchListener(new View.OnTouchListener(){

				@Override
				public boolean onTouch(View p1, MotionEvent p2) {
					closeKeyboard();
					return false;
				}

		});
    }
	
	private Handler appListHandler = new Handler(){

		@Override
		public void handleMessage(Message msg){
			ListView appList = findViewById(android.R.id.list);
			AppListAdapter adapter = (AppListAdapter) appList.getAdapter();
			Object[] args = msg.obj != null && msg.obj instanceof Object[] ? (Object[]) msg.obj : null;
			UserHandle handle = args != null ? (UserHandle) args[0] : null;
			String[] pkgs = handle != null ? (String[]) args[1] : null;
			switch(msg.what){
				case LauncherAppsCallback.ADD:
					adapter.addAll(pkgs,handle);
					adapter.notifyDataSetChanged();
					break;
				case LauncherAppsCallback.REMOVE:
					adapter.remove(pkgs[0]);
					adapter.notifyDataSetChanged();
					break;
				case LauncherAppsCallback.UNAVAILABLE:
					adapter.fillAsync();
					break;
				case LauncherAppsCallback.UPDATE:
					adapter.updateAll(pkgs,handle);
					break;
				case LauncherAppsCallback.FILTER:
					adapter.fillWithFilterResult((String) msg.obj);
					break;
			}
			removeMessages(msg.what);
		}

	};
	

	public void onClick(View p1) {
		View list = findViewById(android.R.id.custom);
		list.setVisibility(list.isShown() ? View.GONE : View.VISIBLE);
		closeKeyboard();
	}
	
	@Override
	public void onItemClick(AdapterView<?> p1, View p2, int p3, long p4){
		LauncherActivityInfo info = (LauncherActivityInfo) p1.getItemAtPosition(Math.max(0,p3));
		startSafe(info);
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> p1, View p2, int p3, long p4) {
		LauncherActivityInfo info = (LauncherActivityInfo) p1.getItemAtPosition(Math.max(0,p3));
		Intent appInfo = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
		appInfo.setData(Uri.parse("package:"+info.getApplicationInfo().packageName));
		startActivity(appInfo);
		return true;
	}
	
	public void getAppOptionsDialog(LauncherActivityInfo info){
		if(mAppOptionsDialog == null){
			mAppOptionsDialog = new AlertDialog.Builder(this);
			
		}
	}
	
	@Override
	public void beforeTextChanged(CharSequence p1, int p2, int p3, int p4) {
	}

	@Override
	public void onTextChanged(CharSequence p1, int p2, int p3, int p4) {
		Message msg = appListHandler.obtainMessage(LauncherAppsCallback.FILTER, p1 != null && p1.length() > 0 
						? p1.toString() 
						: null
					);
		appListHandler.sendMessage(msg);
	}

	@Override
	public void afterTextChanged(Editable p1) {
	}

	@Override
	public void onPause() {
		super.onPause();
		final EditText et = findViewById(android.R.id.edit);
		et.setText("");
		closeKeyboard();
	}

	private void startSafe(LauncherActivityInfo info){
		startSafe(info, null);
	}

	private void startSafe(LauncherActivityInfo info, Bundle opts){
		try {
			mLauncherApps.startMainActivity(info.getComponentName(), info.getUser(), null, opts);
		} catch(Throwable t){
			Log.d("ActList","Exception",t);
			try {
				startActivity(new Intent().setComponent(info.getComponentName()));
			} catch(Throwable x){
				Log.d("ActList","Exception2",x);
			}
		}
	}
	
	private void sendUpdateRequest(int type, UserHandle user, String... packages){
		Message msg = appListHandler.obtainMessage(type, new Object[]{user, packages});
		appListHandler.sendMessageDelayed(msg, 250);
	}
	
	private void closeKeyboard(){
		final EditText et = findViewById(android.R.id.edit);
		if(et.isFocused()){
			et.setFocusable(false);
			et.setFocusableInTouchMode(false);
			et.setFocusableInTouchMode(true);
			et.setFocusable(true);
		}
	}

	@Override
	protected void onNewIntent(Intent intent){
		super.onRestart();
		onClick(null);
	}

	@Override
	public void onBackPressed(){
		onClick(null);
	}

	private class LauncherAppsCallback extends LauncherApps.Callback {
		
		private static final int ADD = 0, UPDATE = 1, REMOVE = 2, UNAVAILABLE = 3, FILTER = 4;
		
        @Override
        public void onPackageChanged(String packageName, UserHandle user) {
			sendUpdateRequest(UPDATE, user, packageName);
        }

        @Override
        public void onPackageRemoved(String packageName, UserHandle user) {
			sendUpdateRequest(REMOVE, user, packageName);
        }

        @Override
        public void onPackageAdded(String packageName, UserHandle user) {
			sendUpdateRequest(ADD, user, packageName);
        }

        @Override
        public void onPackagesAvailable(String[] packageNames, UserHandle user, boolean replacing) {
            if (!replacing) {
				sendUpdateRequest(ADD, user, packageNames);
            } else {
                // If we are replacing then just update the packages in the list
				sendUpdateRequest(UPDATE, user, packageNames);
            }
        }

        @Override
        public void onPackagesUnavailable(String[] packageNames, UserHandle user,
										  boolean replacing) {
            if (!replacing) {
				sendUpdateRequest(UNAVAILABLE, user, packageNames);
            }
        }
    }
}
