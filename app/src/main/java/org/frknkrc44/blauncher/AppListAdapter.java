package org.frknkrc44.blauncher;

import android.content.*;
import android.content.pm.*;
import android.os.*;
import android.util.*;
import android.view.*;
import android.widget.*;
import java.lang.reflect.*;
import java.util.*;

public class AppListAdapter extends BaseAdapter {
	
	private final List<LauncherActivityInfo> mObjects = new ArrayList<>();
	private final Context mContext;
	private BackgroundTask mBackgroundTask;
	private String searchFilter;
	
	public AppListAdapter(Context ctx){
		mContext = ctx;
	}
	
	public void fillAsync(){
		if(mBackgroundTask != null)
			mBackgroundTask.cancel(true);

		mBackgroundTask = new BackgroundTask();
		mBackgroundTask.execute();
	}
	
	public void fillSync(){
		fill();
		notifyDataSetChanged();
	}
	
	private void fill(){
		LauncherApps listProvider = (LauncherApps) getContext().getSystemService(Context.LAUNCHER_APPS_SERVICE);
		List<UserHandle> handles = listProvider.getProfiles();

		for(UserHandle user : handles)
			addAll(listProvider.getActivityList(null, user));
	}
	
	public void fillWithFilterResult(String filter){
		fillWithFilterResult(filter, true);
	}
	
	public void fillWithFilterResult(String filter, boolean async){
		clear();
		searchFilter = filter == null ? null : filter.toLowerCase();
		if(async)
			fillAsync();
		else
			fillSync();
	}
	
	private int getUserId(LauncherActivityInfo info){
		return getUserId(info.getUser());
	}
	
	private int getUserId(UserHandle user){
		try {
			Method getId = UserHandle.class.getDeclaredMethod("getIdentifier");
			getId.setAccessible(true);
			return (int) getId.invoke(user);
		} catch(Throwable t){}
		return -1;
	}
	
	private boolean isOtherUser(LauncherActivityInfo info){
		return isOtherUser(info.getUser());
	}
	
	private boolean isOtherUser(UserHandle user){
		int id = getUserId(user);
		return id > 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if(convertView == null){
			convertView = createView(position, parent);
		}
		
		if(position < 0){
			position = 0;
		}
		
		LauncherActivityInfo info = getItem(position);
		if(info != null){
			TextView title = convertView.findViewById(android.R.id.text1);
			title.setText(info.getLabel());

			TextView text = convertView.findViewById(android.R.id.text2);
			text.setText((isOtherUser(info) ?  "W " : "") + info.getApplicationInfo().packageName);

			TwoLineListItem texts = (TwoLineListItem) ((ViewGroup)convertView).getChildAt(1);
			texts.setGravity(Gravity.CENTER);
			int pad = dpI(16);
			texts.setPadding(pad,0,pad,0);
			TwoLineListItem.LayoutParams tlp = (TwoLineListItem.LayoutParams) title.getLayoutParams();
			tlp.setMargins(0,0,0,0);

			ImageView icon = convertView.findViewById(android.R.id.icon);
			icon.setVisibility(View.GONE);
			// icon.setImageDrawable(info.getIcon((int)(convertView.getResources().getDisplayMetrics().densityDpi * 0.5f)));
		}
		
		return convertView;
	}
	
	private int dpI(float br){
		return (int) dp(br);
	}
	
	private float dp(float br){
		return TypedValue.applyDimension(
			TypedValue.COMPLEX_UNIT_DIP,
			br,
			getContext().getResources().getDisplayMetrics()
		);
	}
	
	private View createView(int position, ViewGroup parent){
		return LayoutInflater.from(getContext()).inflate(R.layout.app_child, parent, false);
	}
	
	public Context getContext(){
		return mContext;
	}

	public LauncherActivityInfo getItem(int index){
		if(getCount() > 0){
			int min = Math.min(index,getCount());
			if(min < getCount())
				return mObjects.get(min);
		}
		return null;
	}
	
	public void clear(){
		mObjects.clear();
	}
	
	public void updateAll(String... packageNames, UserHandle user){
		for(String name : packageNames)
			update(name, user);
	}
	
	public void update(String packageName, UserHandle user){
		remove(packageName);
		add(packageName, user);
		notifyDataSetChanged();
	}
	
	public void remove(String packageName){
		if(packageName == null)
			return;

		for(LauncherActivityInfo info : mObjects){
			if(packageName.equals(info.getApplicationInfo().packageName)){
				remove(info);
				break;
			}
		}
	}

	public void remove(LauncherActivityInfo info){
		if(info != null)
			mObjects.remove(info);
	}

	public void remove(int index){
		if(index > 0)
			mObjects.remove(index);
	}
	
	public void add(String packageName, UserHandle user){
		if(packageName != null){
			LauncherApps listProvider = (LauncherApps) getContext().getSystemService(Context.LAUNCHER_APPS_SERVICE);
			addAll(listProvider.getActivityList(packageName, user));
		}
	}

	public void add(LauncherActivityInfo info){
		if(info != null && !isExists(info))
			if(searchFilter == null || info.getLabel().toString().toLowerCase().startsWith(searchFilter))
				mObjects.add(info);
	}
	
	public void addAll(String... packageNames, UserHandle user){
		for(String name : packageNames)
			add(name, user);
	}

	public void addAll(Collection<LauncherActivityInfo> infos){
		for(LauncherActivityInfo info : infos)
			add(info);
	}
	
	private boolean isExists(LauncherActivityInfo info){
		for(LauncherActivityInfo item : mObjects)
			if(item.getComponentName().equals(info.getComponentName()) && item.getUser().equals(info.getUser()))
				return true;

		return false;
	}

	@Override
	public int getCount(){
		return mObjects.size();
	}

	@Override
	public long getItemId(int p1){
		return 0;
	}

	@Override
	public void notifyDataSetChanged(){
		Collections.sort(mObjects, new AppListComparator());
		Collections.sort(mObjects, new AppListUserComparator());
		super.notifyDataSetChanged();
	}
	
	private class AppListUserComparator implements Comparator<LauncherActivityInfo> {

		@Override
		public int compare(LauncherActivityInfo p1, LauncherActivityInfo p2) {
			return Integer.valueOf(getUserId(p1)).compareTo(getUserId(p2));
		}

	}
	
	private class AppListComparator implements Comparator<LauncherActivityInfo> {

		@Override
		public int compare(LauncherActivityInfo p1, LauncherActivityInfo p2) {
			return firstUpper(p1).compareTo(firstUpper(p2));
		}

		private String firstUpper(LauncherActivityInfo info){
			String temp = info.getLabel().toString();
			return temp.toLowerCase();
		}
	}
	
	private class BackgroundTask extends AsyncTask<Void,Void,Void> {

		@Override
		protected Void doInBackground(Void[] p1){
			fill();
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			notifyDataSetChanged();
			super.onPostExecute(result);
		}

	}
	
}
